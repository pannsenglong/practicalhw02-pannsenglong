import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit
{   
  myForm: FormGroup    
  constructor(private formBuilder: FormBuilder) { }
  
  ngOnInit(){     
    this.myForm = this.formBuilder.group({  
      firstName: new FormControl('', [Validators.required, Validators.maxLength(32)]),
      lastName: new FormControl('', [Validators.required, Validators.maxLength(32)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      areaCode: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
      phoneNumber: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
      password: new FormControl('', [Validators.required, Validators.maxLength(128), Validators.minLength(8)]),
      confirmPassword: new FormControl('', [Validators.required, Validators.maxLength(128), Validators.minLength(8)]),

      address: new FormControl('', Validators.required), 
      address2: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      state: new FormControl('', Validators.required),
      zipCode: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
      isAccept: new FormControl('', Validators.requiredTrue),
    }, {
      validator: isMatch("password","confirmPassword")
     }) 
  } 

  submit(): void {
    console.log(this.myForm.errors);
  }  

  //TODO: Get First Name
  get firstName() {
    return (this.myForm.get('firstName'))
  }

  // TODO: Get Last Name
  get lastName(){ 
    return (this.myForm.get('lastName'))
  }

  // TODO: Get Email
  get email(){
    return (this.myForm.get('email'))
  }

  //TODO: Get Area Code 
  get areaCode(){
    return (this.myForm.get('areaCode'))
  }
 
  //TODO: Get Phone Number  
  get phoneNumber(){
    return (this.myForm.get('phoneNumber'))
  }

  //TODO: Get Address 
  get address(){
    return (this.myForm.get('address'))
  }

  //TODO: Get Address2 
  get address2(){
    return (this.myForm.get('address2'))
  }

  //TODO: Get City
  get city(){
    return (this.myForm.get('city'))
  }

  //TODO: Get State 
  get state(){
    return (this.myForm.get('state'))
  }
 
  //TODO: Get zipCode 
  get zipCode(){
    return (this.myForm.get('zipCode'))
  }
  
  //TODO: Get isAccept 
  get isAccept(){
    return (this.myForm.get('isAccept'))
  }

  //TODO: Get password 
  get password(){
    return (this.myForm.get('password'))
  }

  //TODO: Get Confirm Password
  get confirmPassword(){
    return (this.myForm.get('confirmPassword'))
  }
}   
          

// function isMatch(control1: string, control2: string) 
// {
//   return (formGroup: FormGroup) => {
//       const value1 = formGroup.controls[control1];
//       const value2 = formGroup.controls[control2];

//       if (value1.errors && !value2.errors.mustMatch) {
//           return;
//       }   

//       if (value1.value !== value2.value) {
//           value2.setErrors({ mustMatch: true });
//       } else {
//           value2.setErrors(null);
//       }
//   }
// }

function isMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ mustMatch: true });
      } else {
          matchingControl.setErrors(null);
      }
  }
}